<?php // $Id$

/**
 * @file
 * Provides functions to render menu call backs.
 */

function markup_demo_markupdemo() {

  // Messages
  drupal_set_message(t('This is a status message.'), 'status');
  drupal_set_message(t('This is a warning message.'), 'warning');
  drupal_set_message(t('This is an error message.'), 'error');

  // Dummy content
  $title = t('Vivamus pharetra posuere sapien');
  $content .= '<p>Nulla facilisi. Nulla libero. Vivamus pharetra posuere sapien. Nam consectetuer. Sed aliquam, nunc eget euismod ullamcorper, lectus nunc ullamcorper orci, fermentum bibendum enim nibh eget ipsum. Donec porttitor ligula eu dolor. Maecenas vitae nulla consequat libero cursus venenatis. Nam magna enim, accumsan eu, blandit sed, blandit a, eros.</p>';
  $content .= '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Morbi commodo, ipsum sed pharetra gravida, orci magna rhoncus neque, id pulvinar odio lorem non turpis. Nullam sit amet enim. Suspendisse id velit vitae ligula volutpat condimentum. Aliquam erat volutpat. Sed quis velit.</p>';

  // Dummy links
  $links = array();
  $links['link1'] = array(
    'title' => t('Posuere sapien'),
    'href' => 'admin/build/themes/settings/markupdemo',
  );
  $links['link2'] = array(
    'title' => t('Vivamus pharetra'),
    'href' => 'admin/build/themes/settings/markupdemo'
  );
  $links['link3'] =  array(
    'title' => t('Ipsum sed pharetra'),
    'href' => 'admin/build/themes/settings/markupdemo'
  );

  // Dummy items
  $items = array();

  $items['item1'] = l(t('Item 1 displaying a link'), '/');
  $items['item2'] = l(t('Item 2 link'), '/') . t(' and text');
  $items['item3'] = '<h2>'. t('Item displaying more content') .'</h2><p>Morbi commodo, ipsum sed pharetra gravida, orci magna rhoncus neque, id pulvinar odio lorem non turpis.</p>';

  // Wrapper div
  $output = '<div class="markp-demo-wrapper">';

  // Table
  $header = array(
    array('data' => t('Username'), 'field' => 'u.name'),
    array('data' => t('Status'), 'field' => 'u.status'),
    t('Roles'),
    array('data' => t('Member for'), 'field' => 'u.created', 'sort' => 'desc'),
    array('data' => t('Last access'), 'field' => 'u.access')
  );
  $rows = array(
    array('Jane Facilisi', 'Active', 'Author', '8 months', 'Jan, 3rd 2010'),
    array('Nam Magna ', 'Active', 'Author', '3 years', 'Jan, 3rd 2010'),
    array('Jane Magna', 'Active', 'Admin', '2 weeks', 'Jan, 3rd 2010'),
    array('Sed Dliquam', 'Active', 'Author', '1 years', 'Jan, 3rd 2010'),
    array('Nam Consectetuer', 'Active', 'Admin', '2 years', 'Jan, 3rd 2010'),
    array('John Facilisi', 'Active', 'Author', '6 months', 'Feb, 5th 2010')
  );

  $output .= '<h2 class="markup-demo-label">Table - theme_table()</h2>';
  $output .= theme('table', $header, $rows);

  // Box
  $output .= '<h2 class="markup-demo-label">Box - theme_box()</h2>';
  $output .= theme('box', $title , $content);

  // Links
  $output .= '<h2 class="markup-demo-label">Links - theme_links()</h2>';
  $output .= theme('links', $links);

  // Item list
  $output .= '<h2 class="markup-demo-label">Item list - theme_item_list()</h2>';
  $output .= theme('item_list', $items);

  // Menus
  $output .= '<div class="markup-demo-third"><h2 class="markup-demo-label">'. t('Navigation') .'</h2>'. menu_tree() .'</div>';
  if (menu_tree('primary-links')) {
    $output .= '<div class="markup-demo-third"><h2 class="markup-demo-label">'. t('Primary links') .'</h2>'. menu_tree('primary-links') .'</div>';
  }
  if (menu_tree('secondary-links')) {
    $output .= '<div class="markup-demo-third"><h2 class="markup-demo-label">'. t('Secondary links') .'</h2>'.  menu_tree('secondary-links') .'</div>';
  }

  // Teaser
  $result = markup_demo_node_query();
  $num_rows = FALSE;
  while ($nid = db_fetch_object($result)) {
    // For rendering teaser and pager
    $node = (array) node_load($nid->nid);
    $teaser = node_view($node, TRUE, FALSE);
    $pager = theme('pager', NULL, variable_get('default_nodes_main', 5));
    $num_rows = TRUE;
  }
  $output .= '<h2 class="markup-demo-label">Teaser</h2>';
  $output .= $teaser;

  // Pager
  $output .= '<h2 class="markup-demo-label">Pager</h2>';
  if ($num_rows) {
    $output .= $pager;
  }

  // Image
  $output .= '<h2 class="markup-demo-label">Image - theme_image()</h2>';

  $path = drupal_get_path('module', 'markup_demo');
  $image = 'markup-demo.png';
  $alt = 'A dog dressed like a giraffe';
  $title = 'Giraffe';

  $output .= theme('image', $path .'/'. $image, $alt, $title, NULL, FALSE);

  // Close wrapper
  $output .= '</div>';

  return $output;
}

function markup_demo_markupdemo_html() {

  // General HTML
  $output .= <<<EOT
  <h1>Heading 1</h1>
  <h2>Heading 2</h2>
  <h3>Heading 3</h3>
  <h4>Heading 4</h4>
  <p>Lorem ipsum dolor sit amet, <strong>strong/bold</strong> consectetur adipisicing elit, <em>italic text</em> sed do eiusmod tempor <sub>sub text</sub> ut labore et <sup>sup text</sup> magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
  <blockquote>
    <p>Blockquote... Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aliquam erat volutpat. Sed quis velit. Nulla facilisi. Nulla libero. Vivamus pharetra posuere sapien. Nam consectetuer. Sed aliquam, nunc eget euismod ullamcorper, lectus nunc ullamcorper orci, fermentum bibendum enim nibh eget ipsum. Donec porttitor ligula eu dolor. Maecenas vitae nulla consequat libero cursus venenatis. Nam magna enim, accumsan eu, blandit sed, blandit a, eros.</p>
  </blockquote>

  <ul>
    <li>Unordered list test</li>
    <li>Another list element. Lorem ipsum dolor sit amet, consectetur adipisicing elit.</li>
    <li>Yet another element in the list</li>
    <li>Some long text. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Lorem ipsum dolor sit amet, consectetur adipisicing elit.</li>
  </ul>

  <br />
  <br />
  <ol>
    <li>Ordered list</li>
    <li>Here's a nested unordered list
      <ul>
        <li>Nested Unordered list</li>
        <li>Nested ordered list
          <ol>
            <li>The first</li>
            <li>And the second</li>
          </ol>
        </li>
      </ul>
    </li>
    <li>Ordered List item</li>
    <li>Nested Ordered list
      <ol>
        <li>Some point</li>
        <li>Nested Unordered list
          <ul>
            <li>The first</li>
            <li>And the second</li>
          </ul>
        </li>
      </ol>
    </li>
  </ol>


  <table>
    <thead>
      <tr>
        <th>Header 1</th>
        <th>Header 2</th>
        <th>Header 3</th>
        <th>Header 4</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Cell</td>
        <td>Cell</td>
        <td>Cell</td>
        <td>Cell</td>
      </tr>
      <tr>
        <td>Cell</td>
        <td>Cell</td>
        <td>Cell</td>
        <td>Cell</td>
      </tr>
      <tr>
        <td>Cell</td>
        <td>Cell</td>
        <td>Cell</td>
        <td>Cell</td>
      </tr>
      <tr>
        <td>Cell</td>
        <td>Cell</td>
        <td>Cell</td>
        <td>Cell</td>
      </tr>
      <tr>
        <td>Cell</td>
        <td>Cell</td>
        <td>Cell</td>
        <td>Cell</td>
      </tr>
      <tr>
        <td>Cell</td>
        <td>Cell</td>
        <td>Cell</td>
        <td>Cell</td>
      </tr>
    </tbody>
  </table>

EOT;

  return $output;
}

function markup_demo_markupdemo_content() {

  // Query of nodes
  $result = db_query('SELECT n.nid, n.type, n.status FROM {node} n WHERE n.status = 1 GROUP BY n.type');
  // Query of a list of content types in the node table. This is necessary to determin whether or not a content type has published nodes.
  $result2 = db_query('SELECT DISTINCT n.type FROM {node} n GROUP BY n.type');
  $types = node_get_types('types');

  $output = '';
  while ($nid = db_fetch_object($result)) {
    $node = node_load($nid->nid);
    foreach ($types as $type => $type_data) {
      if ($type == $node->type) {
        $output .= '<h2 class="markup-demo-label">'. check_plain($type_data->name) . t(' content type') .'</h2>';
        $output .= '<h1>'. check_plain($node->title) .'</h1>';
        $output .= node_show($node, NULL);
      }
    }
  }
  // Show content typs that do not have published nodes and provide a link to create these content types.
  foreach ($types as $type => $type_data) {
    $published_type = db_fetch_object($result2);
    if ($type != $published_type->type) {

      $create_type_link = l(t('Create ') . $type_data->name . t(' content.'), 'node/add/'. $type, array('query' => array('destination' => 'admin/build/themes/markupdemo/content')));

      $output .= '<h2 class="markup-demo-label">'. t('Unpublished content type: ') . check_plain($type_data->name) .'</h2>';
      $output .= '<p>There is no content of this type published. </br>'. $create_type_link .'</p>';
    }
  }
  return $output;
}

function markup_demo_markupdemo_forms() {

  // Form elements
  $output .= '<h2 class="markup-demo-label">'. t('Form Elements') .'</h2>';
  $output .= drupal_get_form('markup_demo_form_elements_form');
  
  // Comment form
  $output .= '<h2 class="markup-demo-label">'. t('Comment Form') .'</h2>';
  $output .= comment_form_box(array('nid' => $node->nid), t('Post new comment'));

  // Node form
  $result = markup_demo_node_query();
  while ($nid = db_fetch_object($result)) {
    $node = (object) node_load($nid->nid);
  }

  module_load_include('inc', 'node', 'node.pages');
  $new_node = (object) $new_node;
  $new_node->uid = '';
  $new_node->name = '';
  $new_node->type = $node->type;

  $output .= '<h2 class="markup-demo-label">'. t('Node Form: ') . $node->type .'</h2>';
  if ($node->type != '') {
    $output .= drupal_get_form($node->type .'_node_form', $new_node);
  }

  return $output;
}

function markup_demo_markupdemo_search() {

  $keys = 'the';
  $search_results = search_data($keys, 'node');

  $output .= drupal_get_form('search_form', NULL, $keys, $type);

  if ($search_results) {
    $output .= theme('box', t('Search results'), $search_results);
  }
  else {
    $output .= theme('box', t('Your search yielded no results'), search_help('search#noresults', drupal_help_arg()));
  }

  return $output;
}

function markup_demo_markupdemo_empty() {

  return '';
}
