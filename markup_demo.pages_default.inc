<?php // $Id$
/**
 * @file
 * Add default Panels pages
 */

/**
 * Implementation of hook_default_page_manager_handlers().
 */
function markup_demo_default_page_manager_handlers() {
  $handlers = array();

  // handler export code
  $handler = new stdClass;
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_markup_demo_panel_context_2';
  $handler->task = 'page';
  $handler->subtask = 'markup_demo_panels';
  $handler->handler = 'panel_context';
  $handler->weight = 1;
  $handler->conf = array(
    'title' => 'Panels',
    'no_blocks' => 0,
    'pipeline' => 'ipe',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
  );
  $display = new panels_display;
  $display->layout = 'twocol_stacked';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'middle' => NULL,
      'top' => NULL,
      'left' => NULL,
      'right' => NULL,
      'bottom' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'Markup Demo';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass;
    $pane->pid = 'new-1';
    $pane->panel = 'left';
    $pane->type = 'custom';
    $pane->subtype = 'custom';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'admin_title' => '',
      'title' => 'Custom Panel Pane',
      'body' => '<p>Grasping the sill I pulled myself up to a sitting posture without looking into the building, and gazed down at the baffled animal beneath me.  My exultation was short-lived, however, for scarcely had I gained a secure seat upon the sill than a huge hand grasped me by the neck from behind and dragged me violently into the room.  Here I was thrown upon my back, and beheld standing over me a colossal ape-like creature, white and hairless except for an enormous shock of bristly hair upon its head.</p>

    <p>The thing, which more nearly resembled our earthly men than it did the Martians I had seen, held me pinioned to the ground with one huge foot, while it jabbered and gesticulated at some answering creature behind me.  This other, which was evidently its mate, soon came toward us, bearing a mighty stone cudgel with which it evidently intended to brain me.</p>

    <p>(Excerpt From Edgar Rice Burrough\'s title: A Princess of Mars)</p>',
      'format' => '1',
      'substitute' => TRUE,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $display->content['new-1'] = $pane;
    $display->panels['left'][0] = 'new-1';
    $pane = new stdClass;
    $pane->pid = 'new-2';
    $pane->panel = 'left';
    $pane->type = 'node';
    $pane->subtype = 'node';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'nid' => '85',
      'links' => 0,
      'leave_node_title' => 0,
      'identifier' => '',
      'build_mode' => 'full',
      'link_node_title' => 0,
      'override_title' => 1,
      'override_title_text' => 'Example node',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $display->content['new-2'] = $pane;
    $display->panels['left'][1] = 'new-2';
    $pane = new stdClass;
    $pane->pid = 'new-3';
    $pane->panel = 'right';
    $pane->type = 'custom';
    $pane->subtype = 'custom';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'admin_title' => '',
      'title' => 'Custom Panel Pane (system block style)',
      'body' => 'Grasping the sill I pulled myself up to a sitting posture without looking into the building, and gazed down at the baffled animal beneath me. My exultation was short-lived, however, for scarcely had I gained a secure seat upon the sill than a huge hand grasped me by the neck from behind and dragged me violently into the room. Here I was thrown upon my back, and beheld standing over me a colossal ape-like creature, white and hairless except for an enormous shock of bristly hair upon its head.

    The thing, which more nearly resembled our earthly men than it did the Martians I had seen, held me pinioned to the ground with one huge foot, while it jabbered and gesticulated at some answering creature behind me. This other, which was evidently its mate, soon came toward us, bearing a mighty stone cudgel with which it evidently intended to brain me.

    (Excerpt From Edgar Rice Burrough\'s title: A Princess of Mars)',
      'format' => '1',
      'substitute' => TRUE,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
      'style' => 'block',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $display->content['new-3'] = $pane;
    $display->panels['right'][0] = 'new-3';
    $pane = new stdClass;
    $pane->pid = 'new-4';
    $pane->panel = 'right';
    $pane->type = 'custom';
    $pane->subtype = 'custom';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'admin_title' => '',
      'title' => 'Custom Panel Pane (Rounded Corners style)',
      'body' => 'The thing, which more nearly resembled our earthly men than it did the Martians I had seen, held me pinioned to the ground with one huge foot, while it jabbered and gesticulated at some answering creature behind me. This other, which was evidently its mate, soon came toward us, bearing a mighty stone cudgel with which it evidently intended to brain me.

    (Excerpt From Edgar Rice Burrough\'s title: A Princess of Mars)',
      'format' => '1',
      'substitute' => TRUE,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
      'style' => 'rounded_corners',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $display->content['new-4'] = $pane;
    $display->panels['right'][1] = 'new-4';
    $pane = new stdClass;
    $pane->pid = 'new-5';
    $pane->panel = 'right';
    $pane->type = 'custom';
    $pane->subtype = 'custom';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'admin_title' => '',
      'title' => 'Custom Panel Pane (Custom Style)',
      'body' => 'The thing, which more nearly resembled our earthly men than it did the Martians I had seen, held me pinioned to the ground with one huge foot, while it jabbered and gesticulated at some answering creature behind me. This other, which was evidently its mate, soon came toward us, bearing a mighty stone cudgel with which it evidently intended to brain me.

    (Excerpt From Edgar Rice Burrough\'s title: A Princess of Mars)',
      'format' => '1',
      'substitute' => TRUE,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => array(
        'style' => '$',
        'settings' => array(
          'name' => '_temporary',
          'style_base' => 'pane_plain_box',
          'palette' => array(
            'background' => '#f8f7f7',
            'text' => '#9834b2',
            'border' => '#66ff76',
            'header-background' => '',
            'header-text' => '#99246f',
            'header-border' => '',
          ),
          'font' => array(
            'font' => '',
            'size' => '',
            'letter_spacing' => '',
            'word_spacing' => '',
            'decoration' => '',
            'weight' => '',
            'style' => '',
            'variant' => '',
            'case' => '',
            'alignment' => '',
          ),
          'header_font' => array(
            'font' => '',
            'size' => '',
            'letter_spacing' => '',
            'word_spacing' => '',
            'decoration' => '',
            'weight' => 'bold',
            'style' => 'italic',
            'variant' => '',
            'case' => '',
            'alignment' => '',
          ),
          'border' => array(
            'thickness' => '2px',
            'style' => 'dotted',
          ),
          'header_border' => array(
            'thickness' => 'none',
            'style' => '',
          ),
          'padding' => array(
            'top' => '',
            'right' => '0.7em',
            'bottom' => '',
            'left' => '0.7em',
          ),
        ),
      ),
      'style' => 'stylizer',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 2;
    $display->content['new-5'] = $pane;
    $display->panels['right'][2] = 'new-5';
    $pane = new stdClass;
    $pane->pid = 'new-6';
    $pane->panel = 'right';
    $pane->type = 'block';
    $pane->subtype = 'user-3';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => 'Who\'s Online (block w/ No Style)',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
      'style' => 'default',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 3;
    $display->content['new-6'] = $pane;
    $display->panels['right'][3] = 'new-6';
    $pane = new stdClass;
    $pane->pid = 'new-7';
    $pane->panel = 'right';
    $pane->type = 'block';
    $pane->subtype = 'user-3';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => 'Who\'s Online (block w/ System block style)',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
      'style' => 'block',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 4;
    $display->content['new-7'] = $pane;
    $display->panels['right'][4] = 'new-7';
    $pane = new stdClass;
    $pane->pid = 'new-8';
    $pane->panel = 'right';
    $pane->type = 'block';
    $pane->subtype = 'user-3';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => 'Who\'s Online (block w/ Rounded Corners style)',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
      'style' => 'rounded_corners',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 5;
    $display->content['new-8'] = $pane;
    $display->panels['right'][5] = 'new-8';
    $pane = new stdClass;
    $pane->pid = 'new-9';
    $pane->panel = 'right';
    $pane->type = 'block';
    $pane->subtype = 'user-3';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => 'Who\'s Online (block /w Custom Style)',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => array(
        'style' => '$',
        'settings' => array(
          'name' => '_temporary',
          'style_base' => 'pane_rounded_shadow',
          'palette' => array(
            'background' => '#edcfe9',
            'text' => '#8b5c93',
            'header-text' => '#b635ac',
            'header-border' => '#ff42ea',
          ),
          'header_font' => array(
            'font' => '',
            'size' => '',
            'letter_spacing' => '',
            'word_spacing' => '',
            'decoration' => '',
            'weight' => '',
            'style' => '',
            'variant' => '',
            'case' => '',
            'alignment' => '',
          ),
          'text_font' => array(
            'font' => '',
            'size' => '',
            'letter_spacing' => '',
            'word_spacing' => '',
            'decoration' => '',
            'weight' => '',
            'style' => '',
            'variant' => '',
            'case' => '',
            'alignment' => '',
          ),
          'padding' => array(
            'top' => '',
            'right' => '',
            'bottom' => '',
            'left' => '',
          ),
          'header_border' => array(
            'thickness' => '2px',
            'style' => '',
          ),
        ),
      ),
      'style' => 'stylizer',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 6;
    $display->content['new-9'] = $pane;
    $display->panels['right'][6] = 'new-9';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  
  $handlers[$handler->name] = $handler;
  
  
  
  
  $handler = new stdClass;
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_markup_demo_views_panel_context_2';
  $handler->task = 'page';
  $handler->subtask = 'markup_demo_views';
  $handler->handler = 'panel_context';
  $handler->weight = 1;
  $handler->conf = array(
    'title' => 'Views',
    'no_blocks' => 0,
    'pipeline' => 'ipe',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
  );
  $display = new panels_display;
  $display->layout = 'onecol';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'middle' => NULL,
      'top' => NULL,
      'left' => NULL,
      'right' => NULL,
      'bottom' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'Markup Demo';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass;
    $pane->pid = 'new-1';
    $pane->panel = 'middle';
    $pane->type = 'views_panes';
    $pane->subtype = 'markup_demo-panel_pane_1';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $display->content['new-1'] = $pane;
    $display->panels['middle'][0] = 'new-1';
    $pane = new stdClass;
    $pane->pid = 'new-2';
    $pane->panel = 'middle';
    $pane->type = 'views_panes';
    $pane->subtype = 'markup_demo-panel_pane_2';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $display->content['new-2'] = $pane;
    $display->panels['middle'][1] = 'new-2';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;

  $handlers[$handler->name] = $handler;

  return $handlers;
}

/**
 * Implementation of hook_default_page_manager_pages().
 */
function markup_demo_default_page_manager_pages() {

  $pages = array();

  // page export code
  $page = new stdClass;
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'markup_demo_panels';
  $page->task = 'page';
  $page->admin_title = 'Markup Demo - Panels';
  $page->admin_description = '';
  $page->path = 'admin/build/themes/markupdemo/panels';
  $page->access = array();
  $page->menu = array(
    'type' => 'tab',
    'title' => 'Panels',
    'name' => 'navigation',
    'weight' => '0',
    'parent' => array(
      'type' => 'none',
      'title' => '',
      'name' => 'navigation',
      'weight' => '0',
    ),
  );
  $page->arguments = array();
  $page->conf = array();
  $page->default_handlers = array();
  $handler = new stdClass;
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_markup_demo_panels_panel_context';
  $handler->task = 'page';
  $handler->subtask = 'markup_demo_panels';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panels',
    'no_blocks' => 0,
    'pipeline' => 'ipe',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
  );
  $display = new panels_display;
  $display->layout = 'twocol_stacked';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'middle' => NULL,
      'top' => NULL,
      'left' => NULL,
      'right' => NULL,
      'bottom' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'Markup Demo';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass;
    $pane->pid = 'new-1';
    $pane->panel = 'left';
    $pane->type = 'custom';
    $pane->subtype = 'custom';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'admin_title' => '',
      'title' => 'Custom Panel Pane',
      'body' => '<p>Grasping the sill I pulled myself up to a sitting posture without looking into the building, and gazed down at the baffled animal beneath me.  My exultation was short-lived, however, for scarcely had I gained a secure seat upon the sill than a huge hand grasped me by the neck from behind and dragged me violently into the room.  Here I was thrown upon my back, and beheld standing over me a colossal ape-like creature, white and hairless except for an enormous shock of bristly hair upon its head.</p>

    <p>The thing, which more nearly resembled our earthly men than it did the Martians I had seen, held me pinioned to the ground with one huge foot, while it jabbered and gesticulated at some answering creature behind me.  This other, which was evidently its mate, soon came toward us, bearing a mighty stone cudgel with which it evidently intended to brain me.</p>

    <p>(Excerpt From Edgar Rice Burrough\'s title: A Princess of Mars)</p>',
      'format' => '1',
      'substitute' => TRUE,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $display->content['new-1'] = $pane;
    $display->panels['left'][0] = 'new-1';
    $pane = new stdClass;
    $pane->pid = 'new-2';
    $pane->panel = 'left';
    $pane->type = 'node';
    $pane->subtype = 'node';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'nid' => '85',
      'links' => 0,
      'leave_node_title' => 0,
      'identifier' => '',
      'build_mode' => 'full',
      'link_node_title' => 0,
      'override_title' => 1,
      'override_title_text' => 'Example node',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $display->content['new-2'] = $pane;
    $display->panels['left'][1] = 'new-2';
    $pane = new stdClass;
    $pane->pid = 'new-3';
    $pane->panel = 'right';
    $pane->type = 'custom';
    $pane->subtype = 'custom';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'admin_title' => '',
      'title' => 'Custom Panel Pane (system block style)',
      'body' => 'Grasping the sill I pulled myself up to a sitting posture without looking into the building, and gazed down at the baffled animal beneath me. My exultation was short-lived, however, for scarcely had I gained a secure seat upon the sill than a huge hand grasped me by the neck from behind and dragged me violently into the room. Here I was thrown upon my back, and beheld standing over me a colossal ape-like creature, white and hairless except for an enormous shock of bristly hair upon its head.

    The thing, which more nearly resembled our earthly men than it did the Martians I had seen, held me pinioned to the ground with one huge foot, while it jabbered and gesticulated at some answering creature behind me. This other, which was evidently its mate, soon came toward us, bearing a mighty stone cudgel with which it evidently intended to brain me.

    (Excerpt From Edgar Rice Burrough\'s title: A Princess of Mars)',
      'format' => '1',
      'substitute' => TRUE,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
      'style' => 'block',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $display->content['new-3'] = $pane;
    $display->panels['right'][0] = 'new-3';
    $pane = new stdClass;
    $pane->pid = 'new-4';
    $pane->panel = 'right';
    $pane->type = 'custom';
    $pane->subtype = 'custom';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'admin_title' => '',
      'title' => 'Custom Panel Pane (Rounded Corners style)',
      'body' => 'The thing, which more nearly resembled our earthly men than it did the Martians I had seen, held me pinioned to the ground with one huge foot, while it jabbered and gesticulated at some answering creature behind me. This other, which was evidently its mate, soon came toward us, bearing a mighty stone cudgel with which it evidently intended to brain me.

    (Excerpt From Edgar Rice Burrough\'s title: A Princess of Mars)',
      'format' => '1',
      'substitute' => TRUE,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
      'style' => 'rounded_corners',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $display->content['new-4'] = $pane;
    $display->panels['right'][1] = 'new-4';
    $pane = new stdClass;
    $pane->pid = 'new-5';
    $pane->panel = 'right';
    $pane->type = 'custom';
    $pane->subtype = 'custom';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'admin_title' => '',
      'title' => 'Custom Panel Pane (Custom Style)',
      'body' => 'The thing, which more nearly resembled our earthly men than it did the Martians I had seen, held me pinioned to the ground with one huge foot, while it jabbered and gesticulated at some answering creature behind me. This other, which was evidently its mate, soon came toward us, bearing a mighty stone cudgel with which it evidently intended to brain me.

    (Excerpt From Edgar Rice Burrough\'s title: A Princess of Mars)',
      'format' => '1',
      'substitute' => TRUE,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => array(
        'style' => '$',
        'settings' => array(
          'name' => '_temporary',
          'style_base' => 'pane_plain_box',
          'palette' => array(
            'background' => '#f8f7f7',
            'text' => '#9834b2',
            'border' => '#66ff76',
            'header-background' => '',
            'header-text' => '#99246f',
            'header-border' => '',
          ),
          'font' => array(
            'font' => '',
            'size' => '',
            'letter_spacing' => '',
            'word_spacing' => '',
            'decoration' => '',
            'weight' => '',
            'style' => '',
            'variant' => '',
            'case' => '',
            'alignment' => '',
          ),
          'header_font' => array(
            'font' => '',
            'size' => '',
            'letter_spacing' => '',
            'word_spacing' => '',
            'decoration' => '',
            'weight' => 'bold',
            'style' => 'italic',
            'variant' => '',
            'case' => '',
            'alignment' => '',
          ),
          'border' => array(
            'thickness' => '2px',
            'style' => 'dotted',
          ),
          'header_border' => array(
            'thickness' => 'none',
            'style' => '',
          ),
          'padding' => array(
            'top' => '',
            'right' => '0.7em',
            'bottom' => '',
            'left' => '0.7em',
          ),
        ),
      ),
      'style' => 'stylizer',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 2;
    $display->content['new-5'] = $pane;
    $display->panels['right'][2] = 'new-5';
    $pane = new stdClass;
    $pane->pid = 'new-6';
    $pane->panel = 'right';
    $pane->type = 'block';
    $pane->subtype = 'user-3';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => 'Who\'s Online (block w/ No Style)',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
      'style' => 'default',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 3;
    $display->content['new-6'] = $pane;
    $display->panels['right'][3] = 'new-6';
    $pane = new stdClass;
    $pane->pid = 'new-7';
    $pane->panel = 'right';
    $pane->type = 'block';
    $pane->subtype = 'user-3';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => 'Who\'s Online (block w/ System block style)',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
      'style' => 'block',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 4;
    $display->content['new-7'] = $pane;
    $display->panels['right'][4] = 'new-7';
    $pane = new stdClass;
    $pane->pid = 'new-8';
    $pane->panel = 'right';
    $pane->type = 'block';
    $pane->subtype = 'user-3';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => 'Who\'s Online (block w/ Rounded Corners style)',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
      'style' => 'rounded_corners',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 5;
    $display->content['new-8'] = $pane;
    $display->panels['right'][5] = 'new-8';
    $pane = new stdClass;
    $pane->pid = 'new-9';
    $pane->panel = 'right';
    $pane->type = 'block';
    $pane->subtype = 'user-3';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => 'Who\'s Online (block /w Custom Style)',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => array(
        'style' => '$',
        'settings' => array(
          'name' => '_temporary',
          'style_base' => 'pane_rounded_shadow',
          'palette' => array(
            'background' => '#edcfe9',
            'text' => '#8b5c93',
            'header-text' => '#b635ac',
            'header-border' => '#ff42ea',
          ),
          'header_font' => array(
            'font' => '',
            'size' => '',
            'letter_spacing' => '',
            'word_spacing' => '',
            'decoration' => '',
            'weight' => '',
            'style' => '',
            'variant' => '',
            'case' => '',
            'alignment' => '',
          ),
          'text_font' => array(
            'font' => '',
            'size' => '',
            'letter_spacing' => '',
            'word_spacing' => '',
            'decoration' => '',
            'weight' => '',
            'style' => '',
            'variant' => '',
            'case' => '',
            'alignment' => '',
          ),
          'padding' => array(
            'top' => '',
            'right' => '',
            'bottom' => '',
            'left' => '',
          ),
          'header_border' => array(
            'thickness' => '2px',
            'style' => '',
          ),
        ),
      ),
      'style' => 'stylizer',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 6;
    $display->content['new-9'] = $pane;
    $display->panels['right'][6] = 'new-9';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;

  $pages[$page->name] = $page;
  
  
  
  
  
  
  $page = new stdClass;
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'markup_demo_views';
  $page->task = 'page';
  $page->admin_title = 'Markup Demo - Views';
  $page->admin_description = '';
  $page->path = 'admin/build/themes/markupdemo/views';
  $page->access = array();
  $page->menu = array(
    'type' => 'tab',
    'title' => 'Views',
    'name' => 'navigation',
    'weight' => '0',
    'parent' => array(
      'type' => 'none',
      'title' => 'Markup Demo',
      'name' => 'navigation',
      'weight' => '0',
    ),
  );
  $page->arguments = array();
  $page->conf = array();
  $page->default_handlers = array();
  $handler = new stdClass;
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_markup_demo_views_panel_context_2';
  $handler->task = 'page';
  $handler->subtask = 'markup_demo_views';
  $handler->handler = 'panel_context';
  $handler->weight = 1;
  $handler->conf = array(
    'title' => 'Views',
    'no_blocks' => 0,
    'pipeline' => 'ipe',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
  );
  $display = new panels_display;
  $display->layout = 'onecol';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'middle' => NULL,
      'top' => NULL,
      'left' => NULL,
      'right' => NULL,
      'bottom' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'Markup Demo';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass;
    $pane->pid = 'new-1';
    $pane->panel = 'middle';
    $pane->type = 'views_panes';
    $pane->subtype = 'markup_demo-panel_pane_1';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $display->content['new-1'] = $pane;
    $display->panels['middle'][0] = 'new-1';
    $pane = new stdClass;
    $pane->pid = 'new-2';
    $pane->panel = 'middle';
    $pane->type = 'views_panes';
    $pane->subtype = 'markup_demo-panel_pane_2';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $display->content['new-2'] = $pane;
    $display->panels['middle'][1] = 'new-2';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;



  $pages[$page->name] = $page;
  return $pages;
}


